import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarService {
  private carTypesList: any;
  private myCarList: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  constructor() {}


  getMyCars(){
    return of(this.myCarList);
  }

  async postNewCar(newCar: any){
    const currentCars = this.myCarList.value
    const myCarList=  [
      ...currentCars,
      newCar
    ];
    console.log(myCarList)
    this.myCarList.next(myCarList)
  }

  isCarUpdated() {
    return this.myCarList.asObservable();
  }


  get carTypeList() {
    return of(this.carTypesList)
  }


  updateCar(newCarData: any){
    const currentCars = this.myCarList.value;
    const filteredCarList = currentCars.filter((car: any, i: number) => {
      return car.id !== newCarData.id
    });
    const newCarList = [
      ...filteredCarList,
      newCarData
    ]

    this.myCarList.next(newCarList)
  }


  deleteCar(newCarData: any){
    const currentCars = this.myCarList.value;
    const filteredCarList = currentCars.filter((car: any, i: number) => {
      return car.id !== newCarData.id
    });

    this.myCarList.next(filteredCarList)
  }



}
