import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CarService } from 'src/app/services/car.service';
import { carTypes, carColors, carTractions } from 'src/app/shared/enum';

@Component({
  selector: 'app-create-car',
  templateUrl: './create-car.component.html',
  styleUrls: ['./create-car.component.css']
})
export class CreateCarComponent implements OnInit {
  colorControl = new FormControl('primary');
  myCarList: any = [];
  id: number = 0;
  carForm: FormGroup;

  disabled: boolean = true;
  carColors: any = [];
  carTypes: any = [];
  carTractions: any = [];

  constructor(private carService: CarService, private fb: FormBuilder, private _snackBar: MatSnackBar) {
    this.carForm = this.fb.group({
      carColor: new FormControl(null, Validators.required),
      carType: new FormControl(null, Validators.required),
      carTraction: new FormControl(null, Validators.required),
      carOwner: new FormControl('', Validators.required),
      carYear: new FormControl('', Validators.required),
      carPlate: new FormControl('', Validators.required),
    })

    this.carForm.controls['carTraction'].disable();
    this.carForm.valueChanges.subscribe(() => {
      console.log(this.carForm.getRawValue())
    });

    //Dropdown options
    this.carColors = carColors
    this.carTypes = carTypes
    this.carTractions = carTractions
  }

  ngOnInit(): void {
  }

  addNewCar() {
    if(!this.carForm.valid) return this.openSnackBar();
    this.id++;
    let newCar = {
      id: this.id,
      createdAt: new Date,
      ...this.carForm.getRawValue(),
    }
    this.carService.postNewCar(newCar);
    this.getCars();
  }

  getCars() {
    this.carService.getMyCars().subscribe()
  }

  handleTypeSelected(selectedOption: any){
    const carTraction = this.carTractions.find((el:any) => el.carType == selectedOption.value)    
    this.carForm.controls['carTraction'].setValue(carTraction.value.toString())    
  }

  openSnackBar() {
    this._snackBar.open('Complete todos los campos para continuar.');
  }

}
