import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { CreateCarComponent } from './create-car/create-car.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { MainRoutingModule } from './main-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarTypePipe } from 'src/app/pipes/car-type.pipe';
import { CarColorPipe } from 'src/app/pipes/car-color.pipe';
import { CarTractionPipe } from 'src/app/pipes/car-traction.pipe';
import { MyCarListComponent } from './my-car-list/my-car-list.component';
import { EditCarComponent } from './my-car-list/edit-car/edit-car.component';


@NgModule({
  declarations: [
    MainComponent,
    CreateCarComponent,
    MyCarListComponent,
    CarTypePipe,
    CarColorPipe,
    CarTractionPipe,
    EditCarComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents: [
    EditCarComponent,
  ]
})
export class MainModule { }
