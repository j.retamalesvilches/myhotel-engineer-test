import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CarService } from 'src/app/services/car.service';
import { EditCarComponent } from './edit-car/edit-car.component';

@Component({
  selector: 'app-my-car-list',
  templateUrl: './my-car-list.component.html',
  styleUrls: ['./my-car-list.component.css']
})
export class MyCarListComponent implements OnInit {
  displayedColumns: string[] = ['carOwner', 'carPlate', 'carColor', 'carType','carTraction','carYear','actions'];
  myCarList: any = [];
  constructor(private carService: CarService, public dialog: MatDialog) {
    this.carService.isCarUpdated().subscribe(r => {
      this.myCarList = r.sort((a: any, b: any) => parseInt(a.id) - parseInt(b.id));
    })
   }

  ngOnInit(): void {
  }


  handleEdit(car: any){
    this.dialog.open(EditCarComponent, {data: car});
  }

  handleDelete(car: any){
    this.carService.deleteCar(car);
  }


}
