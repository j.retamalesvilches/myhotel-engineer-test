import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CarService } from 'src/app/services/car.service';
import { carColors, carTractions, carTypes } from 'src/app/shared/enum';
@Component({
  selector: 'app-edit-car',
  templateUrl: './edit-car.component.html',
  styleUrls: ['./edit-car.component.css']
})
export class EditCarComponent implements OnInit {
  carForm: FormGroup;
  disabled: boolean = true;
  carColors: any = [];
  carTypes: any = [];
  carTractions: any = [];

  actualCar: any;
  constructor( @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private _snackBar: MatSnackBar, private carService: CarService) { 

    this.carForm = this.fb.group({
      id: new FormControl(0, Validators.required),
      carColor: new FormControl(null, Validators.required),
      carType: new FormControl(null, Validators.required),
      carTraction: new FormControl({ value: null, disabled: this.disabled}, Validators.required),
      carOwner: new FormControl('', Validators.required),
      carYear: new FormControl('', Validators.required),
      carPlate: new FormControl('', Validators.required),
      createdAt: new FormControl(null, Validators.required)
    })

    this.carColors = carColors;
    this.carTypes = carTypes;
    this.carTractions = carTractions;

    this.actualCar = data;
    this.carForm.patchValue(data);
  }

  ngOnInit(): void {
  }


  onNoClick(): void {
  }

  handleTypeSelected(selectedOption: any){
    const carTraction = this.carTractions.find((el:any) => el.carType == selectedOption.value)    
    this.carForm.controls['carTraction'].setValue(carTraction.value.toString())    
  }


  updateCar(){
    // console.log(this.carForm.valid, this.carForm.value)
    if(!this.carForm.valid) return this.openSnackBar();
    this.carService.updateCar(this.carForm.getRawValue());
    
  }


  openSnackBar() {
    this._snackBar.open('Complete todos los campos para continuar.');
  }

}
