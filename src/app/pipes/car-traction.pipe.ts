import { Pipe, PipeTransform } from '@angular/core';
import { carTractions } from '../shared/enum';

@Pipe({
  name: 'carTraction'
})
export class CarTractionPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    const tractions = carTractions; 
    return tractions.find(el => el.value == value)?.name;
  }

}
