import { Pipe, PipeTransform } from '@angular/core';
import { carColors } from '../shared/enum';

@Pipe({
  name: 'carColor'
})
export class CarColorPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    const colors = carColors; 
    return colors.find(el => el.value == value)?.name;
  }

}
