import { Pipe, PipeTransform } from '@angular/core';
import { carTypes } from '../shared/enum';

@Pipe({
  name: 'carType'
})
export class CarTypePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    const types = carTypes; 
    return types.find(el => el.value == value)?.name;
  }

}
