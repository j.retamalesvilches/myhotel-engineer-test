
const  carColors = [
    {value: 0, name: 'Rojo'},
    {value: 1, name: 'Azul'},
    {value: 2, name: 'Blanco'},
    {value: 3, name: 'Gris'},
    {value: 4, name: 'Negro'},
  ]

const  carTypes = [
    {value: 0, name: 'Camioneta'},
    {value: 1, name: 'Turismo'},
    {value: 2, name: 'Vehiculo de ciudad'},
    {value: 3, name: 'Deportivo'},
  ]

const carTractions = [
    {value: 0, name: 'AWD', carType: 0},
    {value: 1, name: 'RWD', carType: 1},
    {value: 2, name: 'FWD', carType: 2},
    {value: 3, name: '4WD', carType: 3},
  ]


export { carColors, carTypes, carTractions}