# MyhotelEngineerTest

Hi, my name is Joaquin Retamales in this repo you'll see a crud proyect based on cars. This proyect is named Car Administrator where you can add, update, delete your cars from the database.


# Proyect's information
First of all, this proyects doesn't use an API to receive and send data, all tha data is handle in the client side using services to share the **Car List** bewteen components also the proyect uses the **Lazy loading** structure but since in this proyect we only have 1 route is hardly used.

Car administrator proyect features the uses of a main component, pipes and enum:
- Car administrator has a main component where all the data will be shown. We have an specific route which is:
	> localhost:4200/app
- All the dropdown options are stored in a **enum** file in the shared folder.
- All the imports from Matertial angular are stored and manage in the **material.module.ts**  
- We implemented pipes to filter the data from the database into the table so we can show names instead of number values.
## Running development server

This proyect uses **Angular 13**, **Material Angular** and **TailwindCSS*.
To run this proyecto you should run:
> npm i

when all the packages are installed run:
> ng s

